package com.tidingo.tidingo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.GeolocationPermissions;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

import static java.util.UUID.randomUUID;

public class MainActivity extends AppCompatActivity {
    private WebView mWebView;
    final int SELECT_PHOTO = 1;
    int SelectFoto = 0;
    private static final int GALLERY_INTENT = 2;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 3;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 4;
    String banda1 = "0";

    private Uri uri;
    private Uri uri2;
    String childUpdates = null;
    String uidNeg;
    String numeroLlamar;
    DatabaseReference refe = FirebaseDatabase.getInstance().getReference();
    StorageReference filepath;
    StorageReference filepath2;
    Bitmap nuevoUri;
    Bitmap nuevoUri2;
    String idNombre;
    Bitmap uriBitmap;
    //firebase auth object
    private ProgressDialog dialogoProgress;
    private ProgressDialog progressDialog;
    private StorageReference storageRef;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mWebView = (WebView) findViewById(R.id.miWeb);

        storageRef = FirebaseStorage.getInstance().getReference();
        firebaseAuth = FirebaseAuth.getInstance();

        Button nuevaconexion = (Button) findViewById(R.id.nuevaconexion);
        dialogoProgress = new ProgressDialog(this);
        progressDialog = new ProgressDialog(this);
        //mWebView.loadUrl("http://detodoapp.com/A)%20NUEVA%20VERSION/");
        if (isNetworkAvailable()) {
            mWebView.setWebViewClient(new WebViewClient());
            mWebView.setWebChromeClient(new WebChromeClient() {
                public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                    callback.invoke(origin, true, false);
                }
            });
            WebSettings webSettings = mWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
            webSettings.setDomStorageEnabled(true);
            webSettings.setAllowFileAccess(true);
            webSettings.setAllowFileAccessFromFileURLs(true);
            webSettings.setAllowUniversalAccessFromFileURLs(true);
            webSettings.setSupportMultipleWindows(true);
            webSettings.setGeolocationEnabled(true);
            webSettings.setPluginState(WebSettings.PluginState.ON);
            CookieManager.setAcceptFileSchemeCookies(true);
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            mWebView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                // chromium, enable hardware acceleration
                mWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            } else {
                // older android version, disable hardware acceleration
                mWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            }

            mWebView.addJavascriptInterface(new MyJavascriptInterface(this), "Android");
            mWebView.setWebViewClient(new WebViewClient() {

                @Override
                public void onPageFinished(WebView view, String url) {
                    //show webview

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            findViewById(R.id.loading).animate()
                                    .translationY(findViewById(R.id.loading).getHeight())
                                    .alpha(0.0f)
                                    .setDuration(600)
                                    .setListener(new AnimatorListenerAdapter() {
                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            super.onAnimationEnd(animation);
                                            findViewById(R.id.loading).setVisibility(View.GONE);

                                        }
                                    });

                            mWebView.setWebViewClient(new WebViewClient() {

                                @Override
                                public void onReceivedError(final WebView view, int errorCode, String description,
                                                            final String failingUrl) {
                                    //control you layout, show something like a retry button, and
                                    //call view.loadUrl(failingUrl) to reload.
                                    super.onReceivedError(view, errorCode, description, failingUrl);
                                    findViewById(R.id.conexion).setVisibility(View.VISIBLE);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            progressDialog.setMessage("Cargando...");
                                            progressDialog.show();
                                            progressDialog.dismiss();
                                        }
                                    });
                                }
                            });

                        }
                    }, 600);

                }

            });

            mWebView.loadUrl("https://detodoapp.com/webview/index.html");
        } else {
            findViewById(R.id.conexion).setVisibility(View.VISIBLE);
        }

        nuevaconexion.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });


    }

    // Function to load all URLs in same webview
    private class CustomWebViewClient extends WebViewClient {
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (!isNetworkAvailable()) {
                findViewById(R.id.conexion).setVisibility(View.VISIBLE);
            } else {
                view.loadUrl(url);
            }
            return true;
        }
    }

    class MyJavascriptInterface {

        Context mContext;

        /** Instantiate the interface and set the context */
        MyJavascriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        /** Show a toast from the web page */
        public void getIds(String uidNe) {
            uidNeg = uidNe;
            SelectFoto = 1;
            //System.out.println(uidNeg);
            if (ActivityCompat.checkSelfPermission(MainActivity.this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                } else {

                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                }
            } else {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, GALLERY_INTENT);
            }

        }

        @JavascriptInterface
        /** Show a toast from the web page */
        public void llamarTel(String llamada) {
            numeroLlamar = llamada;

            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + numeroLlamar));
            if (ActivityCompat.checkSelfPermission(MainActivity.this,
                    android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                        android.Manifest.permission.CALL_PHONE)) {

                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{android.Manifest.permission.CALL_PHONE},
                            MY_PERMISSIONS_REQUEST_CALL_PHONE);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }
            startActivity(callIntent);
        }

        @JavascriptInterface
        public void abrirLink(String link) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
            startActivity(browserIntent);
        }

        @JavascriptInterface
        /** Show a toast from the web page */
        public void getPerfil(String uidNe) {
            uidNeg = uidNe;
            SelectFoto = 2;
            //System.out.println(uidNeg);
            if (ActivityCompat.checkSelfPermission(MainActivity.this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                } else {

                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                }
            } else {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, GALLERY_INTENT);
            }
        }

        @JavascriptInterface
        /** Show a toast from the web page */
        public void cargandoLog(String html) {
            //System.out.println(html);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.setMessage("Cargando...");
                    progressDialog.show();
                }
            });
        }

        @JavascriptInterface
        public void cargadoLo(String cargado) {
            //System.out.println(cargado);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.setMessage("Cargando...");
                    progressDialog.show();
                    progressDialog.dismiss();
                }
            });
        }

        @JavascriptInterface
        public String sample(String temp) {
            return temp;
            //System.out.println(cargado);
        }

        @JavascriptInterface
        public void contraCh(String pal) {
            //System.out.println(pal);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.setMessage("Cargando...");
                    progressDialog.show();
                }
            });
            Toast.makeText(MainActivity.this, pal, Toast.LENGTH_LONG).show();
        }

        @JavascriptInterface
        public void bandera1(String ban1) {
            banda1 = ban1;

        }

        @JavascriptInterface
        /** Show a toast from the web page */
        public void getPortada(String uidNe) {
            uidNeg = uidNe;
            SelectFoto = 3;
            //System.out.println(uidNeg);
            if (ActivityCompat.checkSelfPermission(MainActivity.this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                } else {

                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                }
            } else {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, GALLERY_INTENT);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + numeroLlamar));

                    startActivity(callIntent);
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, GALLERY_INTENT);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public Bitmap redimensionarImagenMaximo(Bitmap mBitmap, float newWidth){
        //Redimensionamos
        float newHeigth;
        int width = mBitmap.getWidth();
        System.out.println(width);
        if(width>newWidth) {
            int height = mBitmap.getHeight();
            float scaleWidth = ((float) newWidth) / width;
            float scaleHeight = scaleWidth;
            // create a matrix for the manipulation

            Matrix matrix = new Matrix();
            // resize the bit map
            matrix.postScale(scaleWidth, scaleHeight);

            // recreate the new Bitmap
            return Bitmap.createBitmap(mBitmap, 0, 0, width, height, matrix, false);
        }else{
            int height = mBitmap.getHeight();
            float scaleWidth = 1;
            float scaleHeight = scaleWidth;
            // create a matrix for the manipulation

            Matrix matrix = new Matrix();
            // resize the bit map
            matrix.postScale(scaleWidth, scaleHeight);

            // recreate the new Bitmap
            return Bitmap.createBitmap(mBitmap, 0, 0, width, height, matrix, false);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_INTENT && resultCode == RESULT_OK && data != null) {
            //System.out.println(uidNeg);

            dialogoProgress.setMessage("Subiendo... ");
            dialogoProgress.show();

            Uri yourUri = data.getData();

            String[] filePath = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(yourUri, filePath, null, null, null);
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            uriBitmap = BitmapFactory.decodeFile(imagePath, options);

            if(uriBitmap==null){
                System.out.println("entra");
                uri=data.getData();
                uri2=data.getData();
            }else {

                System.out.println("entraaqui"+uriBitmap);
                cursor.close();
                if (SelectFoto == 2) {
                    nuevoUri = redimensionarImagenMaximo(uriBitmap, 500);
                    nuevoUri2 = redimensionarImagenMaximo(uriBitmap, 900);
                } else {
                    nuevoUri = redimensionarImagenMaximo(uriBitmap, 700);
                    nuevoUri2 = redimensionarImagenMaximo(uriBitmap, 1366);
                }

                uri = getImageUri(this, nuevoUri);
                uri2 = getImageUri(this, nuevoUri2);
            }
            //uri = data.getData();
            System.out.println(uri.toString());
            if (SelectFoto==1) {
                filepath2 = storageRef.child("users").child(uidNeg).child("img1024").child("todas").child(uri2.getLastPathSegment());
                filepath = storageRef.child("users").child(uidNeg).child("img500").child("todas").child(uri.getLastPathSegment());
            }
            if (SelectFoto==3) {
                filepath2 = storageRef.child("users").child(uidNeg).child("img1024").child("portada").child(uri2.getLastPathSegment());
                filepath = storageRef.child("users").child(uidNeg).child("img500").child("portada").child(uri.getLastPathSegment());
            }
            if (SelectFoto==2) {
                filepath2 = storageRef.child("users").child(uidNeg).child("img1024").child("perfil").child(uri2.getLastPathSegment());
                filepath = storageRef.child("users").child(uidNeg).child("img500").child("perfil").child(uri.getLastPathSegment());
            }
            idNombre = randomUUID().toString();
            //System.out.println();

            filepath2.putFile(uri2).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    childUpdates = taskSnapshot.getDownloadUrl().toString();
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    //refe.child("users").child(uidNeg).child("urlImagen").setValue(childUpdates);

                    if (SelectFoto == 1) {
                        refe.child("users").child(uidNeg).child("fotosImg").child("img1024").child(idNombre).child("nombre").setValue(uri2.getLastPathSegment());
                        refe.child("users").child(uidNeg).child("fotosImg").child("img1024").child(idNombre).child("url").setValue(childUpdates);

                    }
                    if (SelectFoto == 2) {
                        refe.child("users").child(uidNeg).child("imgPerfil").child("img1024").child("nombre").setValue(uri2.getLastPathSegment());
                        refe.child("users").child(uidNeg).child("imgPerfil").child("img1024").child("url").setValue(childUpdates);

                    }
                    if (SelectFoto == 3) {
                        refe.child("users").child(uidNeg).child("imgPortada").child("img1024").child("nombre").setValue(uri2.getLastPathSegment());
                        refe.child("users").child(uidNeg).child("imgPortada").child("img1024").child("url").setValue(childUpdates);

                    }
                }
            });

            filepath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    childUpdates = taskSnapshot.getDownloadUrl().toString();
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    //refe.child("users").child(uidNeg).child("urlImagen").setValue(childUpdates);
                    if (SelectFoto == 1) {
                        refe.child("users").child(uidNeg).child("fotosImg").child("img500").child(idNombre).child("nombre").setValue(uri.getLastPathSegment());
                        refe.child("users").child(uidNeg).child("fotosImg").child("img500").child(idNombre).child("url").setValue(childUpdates);
                        String temp="4";
                        mWebView.loadUrl("javascript:sample('"+temp+"');");
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Do something after 5s = 5000ms
                                dialogoProgress.dismiss();
                                Toast.makeText(MainActivity.this, "Subida completada", Toast.LENGTH_LONG).show();
                            }
                        }, 1000);

                    }
                    if (SelectFoto == 2) {
                        refe.child("users").child(uidNeg).child("imgPerfil").child("img500").child("nombre").setValue(uri.getLastPathSegment());
                        refe.child("users").child(uidNeg).child("imgPerfil").child("img500").child("url").setValue(childUpdates);
                        String temp="6";
                        mWebView.loadUrl("javascript:sample('"+temp+"');");
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Do something after 5s = 5000ms
                                dialogoProgress.dismiss();
                                Toast.makeText(MainActivity.this, "Subida completada", Toast.LENGTH_LONG).show();
                            }
                        }, 1000);
                    }
                    if (SelectFoto == 3) {
                        refe.child("users").child(uidNeg).child("imgPortada").child("img500").child("nombre").setValue(uri.getLastPathSegment());
                        refe.child("users").child(uidNeg).child("imgPortada").child("img500").child("url").setValue(childUpdates);
                        String temp="6";
                        mWebView.loadUrl("javascript:sample('"+temp+"');");
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Do something after 5s = 5000ms
                                dialogoProgress.dismiss();
                                Toast.makeText(MainActivity.this, "Subida completada", Toast.LENGTH_LONG).show();
                            }
                        }, 1000);
                    }
                }
            });

        }
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void onBackPressed() {
        System.out.println(banda1);
        if(banda1.equals("0")) {
            if (mWebView.canGoBack()) {
                mWebView.goBack();
            } else {
                super.onBackPressed();
            }
        }else{
            String temp=banda1;
            mWebView.loadUrl("javascript:sample('"+temp+"');");
        }
    }



}
